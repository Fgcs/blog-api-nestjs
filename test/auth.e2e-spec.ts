import { Test, TestingModule } from '@nestjs/testing';
import { HttpStatus, INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { LoginDto } from 'src/auth/dto/login-auth.dto';
const userDto: CreateUserDto = {
  email: 'test@test.com',
  first_name: 'test',
  last_name: 'test',
  password: 'Test12345678.',
};
const loginDto: LoginDto = {
  email: 'test@test.com',
  password: 'Test12345678.',
};
describe('AppController (e2e)', () => {
  let app: INestApplication;
  let userId: string;
  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.setGlobalPrefix('api');
    await app.init();
  });
  afterAll(async () => {
    await app.close();
  });

  it('should register new user /register (POST)', async () => {
    return await request(app.getHttpServer())
      .post('/api/auth/register')
      .send(userDto)
      .expect(({ body }) => {
        userId = body.id;
        expect(body).toEqual({
          email: userDto.email,
          first_name: userDto.first_name,
          last_name: userDto.last_name,
          id: expect.any(String),
          rol: 'user',
          status: 'active',
          created_at: expect.any(String),
          updated_at: expect.any(String),
        });
        expect(body.password).toBeUndefined();
      })
      .expect(HttpStatus.CREATED);
  });
  it('should reject duplicate registration', async () => {
    return await request(app.getHttpServer())
      .post('/api/auth/register')
      .set('Accept', 'application/json')
      .send(userDto)
      .expect(({ body }) => {
        expect(body.message).toEqual('User with that email already exists');
      })
      .expect(HttpStatus.BAD_REQUEST);
  });

  it('should return a token /login', async () => {
    return await request(app.getHttpServer())
      .post('/api/auth/login')
      .set('Accept', 'application/json')
      .send(loginDto)
      .expect(({ body }) => {
        expect(body.token).toEqual(expect.any(String));
        expect(body.date_login).toEqual(expect.any(String));
        expect(body.expires_in).toEqual(expect.any(String));
        expect(body.token_type).toEqual('Bearer');
      })
      .expect(HttpStatus.CREATED);
  });
  it('should return a error if email not found ', async () => {
    const user = {
      email: 'emailFail@gmail.com',
      password: '123425.',
    };
    return await request(app.getHttpServer())
      .post('/api/auth/login')
      .set('Accept', 'application/json')
      .send(user)
      .expect(({ body }) => {
        expect(body.message).toEqual('user not found');
      })
      .expect(HttpStatus.NOT_FOUND);
  });
  // it('should return a error if user with status block  ', async () => {
  //   const user = {
  //     email: userDto.email,
  //     password: '123425.',
  //   };
  //   return await request(app.getHttpServer())
  //     .post('/api/auth/login')
  //     .set('Accept', 'application/json')
  //     .send(user)
  //     .expect(({ body }) => {
  //       expect(body.message).toEqual('Wrong credentials provided');
  //     })
  //     .expect(HttpStatus.BAD_REQUEST);
  // });
  it('should return a error if password not matches ', async () => {
    const user = {
      email: userDto.email,
      password: '123425.',
    };
    return await request(app.getHttpServer())
      .post('/api/auth/login')
      .set('Accept', 'application/json')
      .send(user)
      .expect(({ body }) => {
        expect(body.message).toEqual('Wrong credentials provided');
      })
      .expect(HttpStatus.BAD_REQUEST);
  });
  it('should delete a user by id', async () => {
    return await request(app.getHttpServer())
      .delete(`/api/users/${userId}`)
      .expect(HttpStatus.OK);
  });
});
