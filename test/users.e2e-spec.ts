import { Test, TestingModule } from '@nestjs/testing';
import { HttpStatus, INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { Connection } from 'typeorm';
import { AppModule } from '../src/app.module';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { UpdateUserDto } from 'src/users/dto/update-user.dto';
const updateUser: UpdateUserDto = {
  email: 'updatetest@test.com',
  first_name: 'test update',
  last_name: 'test test update',
  password: 'Test12345678.',
};
const userDto: CreateUserDto = {
  email: 'test@test.com',
  first_name: 'test',
  last_name: 'test',
  password: 'Test12345678.',
};
describe('UsersControllers (e2e)', () => {
  let app: INestApplication;
  let userId: string;
  let conection: Connection;
  beforeEach(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();
    app = moduleRef.createNestApplication();
    app.setGlobalPrefix('api');
    await app.init();
  });
  afterAll(async () => {
    await conection.close();
    await app.close();
  });
  it('should return an array of users /users (GET)', async () => {
    return await request(app.getHttpServer())
      .get('/api/users')
      .set('Accept', 'application/json')
      .expect(200);
  });
  it('should create a new user /users (POST)', async () => {
    return await request(app.getHttpServer())
      .post('/api/users')
      .send(userDto)
      .set('Accept', 'application/json')
      .expect(({ body }) => {
        expect(body.id).toBeDefined();
        userId = body.id;
        expect(body.first_name).toEqual(userDto.first_name);
        expect(body.email).toEqual(userDto.email);
        expect(body.last_name).toEqual(userDto.last_name);
        expect(body.password).toEqual(expect.any(String));
        expect(body.rol).toBe('user');
        expect(body.status).toBe('active');
        expect(body.created_at).toEqual(expect.any(String));
        expect(body.updated_at).toEqual(expect.any(String));
      })
      .expect(HttpStatus.CREATED);
  });
  it('should get a user by id (GET) ', async () => {
    return await request(app.getHttpServer())
      .get(`/api/users/${userId}`)
      .set('Accept', 'application/json')
      .expect(({ body }) => {
        expect(body.id).toBeDefined();
        expect(body.email).toEqual(userDto.email);
        expect(body.first_name).toEqual(userDto.first_name);
        expect(body.last_name).toEqual(userDto.last_name);
        expect(body.rol).toBe('user');
        expect(body.status).toBe('active');
        expect(body.created_at).toEqual(expect.any(String));
        expect(body.updated_at).toEqual(expect.any(String));
      })
      .expect(HttpStatus.OK);
  });
  it('should get a user by Email (GET) ', async () => {
    return await request(app.getHttpServer())
      .get(`/api/users/email/${userDto.email}`)
      .set('Accept', 'application/json')
      .expect(({ body }) => {
        expect(body.id).toBeDefined();
        expect(body.first_name).toEqual(userDto.first_name);
        expect(body.last_name).toEqual(userDto.last_name);
        expect(body.email).toEqual(userDto.email);
        expect(body.rol).toBe('user');
        expect(body.status).toBe('active');
        expect(body.created_at).toEqual(expect.any(String));
        expect(body.updated_at).toEqual(expect.any(String));
      })
      .expect(HttpStatus.OK);
  });
  it('should update  a  user /users (PATCH)', async () => {
    return await request(app.getHttpServer())
      .patch(`/api/users/${userId}`)
      .send(updateUser)
      .set('Accept', 'application/json')
      .expect(({ body }) => {
        expect(body.id).toBeDefined();
        userId = body.id;
        expect(body.email).toEqual(updateUser.email);
        expect(body.first_name).toEqual(updateUser.first_name);
        expect(body.last_name).toEqual(updateUser.last_name);
        expect(body.password).toEqual(expect.any(String));
        expect(body.rol).toBe('user');
        expect(body.status).toBe('active');
      })
      .expect(HttpStatus.OK);
  });
  it('should delete a user by id', async () => {
    return request(app.getHttpServer())
      .delete(`/api/users/${userId}`)
      .expect(({ body }) => {
        expect(body.id).toBeDefined();
        expect(body.first_name).toEqual(updateUser.first_name);
        expect(body.last_name).toEqual(updateUser.last_name);
        expect(body.email).toEqual(updateUser.email);
        expect(body.rol).toBe('user');
        expect(body.status).toBe('active');
      })
      .expect(HttpStatus.OK);
  });
});
