import {
  Controller,
  Post,
  Body,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { User } from 'src/users/entities/user.entity';
import { AuthService } from './auth.service';
import { LoginDto } from './dto/login-auth.dto';
import { NewPasswordModel } from './dto/newPassword.dto';
import { RegisterDto } from './dto/register-auth.dto';
import { VerifyPasswordDto } from './dto/verify-password.dto';
import { Auth } from './entities/auth.entity';
import { token } from './entities/token.entity';
@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('/login')
  @UsePipes(new ValidationPipe())
  login(@Body() credencials: LoginDto): Promise<Auth> {
    return this.authService.login(credencials);
  }
  @Post('/newPassword')
  @UsePipes(new ValidationPipe())
  changePassword(@Body() newPassword: NewPasswordModel): Promise<User> {
    return this.authService.changePassword(newPassword);
  }
  @Post('/verify-password')
  @UsePipes(new ValidationPipe())
  verify(@Body() password: VerifyPasswordDto): Promise<token> {
    return this.authService.verifyPassword(password);
  }
  @Post('/register')
  @UsePipes(new ValidationPipe())
  register(@Body() credencials: RegisterDto): Promise<User> {
    return this.authService.register(credencials);
  }
}
