import { IsEmail, IsNotEmpty } from 'class-validator';

export class RegisterDto {
  @IsNotEmpty({ message: 'this field is required' })
  first_name: string;
  @IsNotEmpty({ message: 'this field is required' })
  last_name: string;
  @IsNotEmpty({ message: 'this field is required' })
  password: string;
  @IsEmail()
  email: string;
}
