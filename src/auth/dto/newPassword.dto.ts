import { IsEmail, IsNotEmpty } from 'class-validator';
export class NewPasswordModel {
  email?: string;
  token?: string;
  @IsNotEmpty({ message: 'this field is required' })
  password?: string;
}
