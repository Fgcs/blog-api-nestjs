import { IsEmail, IsNotEmpty } from 'class-validator';
export class LoginDto {
  @IsNotEmpty({ message: 'this field is required' })
  @IsEmail()
  email: string;
  @IsNotEmpty({ message: 'this field is required' })
  password: string;
}
