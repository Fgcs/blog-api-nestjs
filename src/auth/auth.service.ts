import {
  HttpException,
  HttpStatus,
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { LoginDto } from './dto/login-auth.dto';
import { RegisterDto } from './dto/register-auth.dto';
import { JwtService } from '@nestjs/jwt';
import { compareSync, hashSync } from 'bcrypt';
import { InjectRepository } from '@nestjs/typeorm';
import { Auth } from './entities/auth.entity';
import { Repository } from 'typeorm';
import { UsersService } from '../users/users.service';
import { User } from 'src/users/entities/user.entity';
import { PostgresErrorCode } from './enum/postgresErrorCodes.enum';
import { NewPasswordModel } from './dto/newPassword.dto';
import { VerifyPasswordDto } from './dto/verify-password.dto';
import { token } from './entities/token.entity';
@Injectable()
export class AuthService {
  constructor(
    private readonly jwtService: JwtService,
    @InjectRepository(Auth) private readonly authDb: Repository<Auth>,
    private readonly usersService: UsersService,
  ) {}
  async login(user: LoginDto): Promise<Auth> {
    const { email, password } = user;
    try {
      const userDB = await this.usersService.getByEmail(email);

      if (userDB.status != 'active')
        throw new NotFoundException(
          'Your user is currently in status ' + userDB.status,
        );

      const isPasswordMatching = await this.comparePassword(
        password,
        userDB.password,
      );

      if (!isPasswordMatching)
        throw new HttpException(
          'Wrong credentials provided',
          HttpStatus.BAD_REQUEST,
        );

      const payload = {
        id: String(userDB.id),
        email: userDB.email,
        rol: userDB.rol,
      };
      const body = {
        date_login: new Date(),
        token_type: 'Bearer',
        token: this.jwtService.sign(payload, {
          expiresIn: '1d',
        }),
        expires_in: (24 * 60 * 60).toString(),
        user: userDB,
      };
      return await this.authDb
        .save(body)
        .then((item) => {
          return {
            date_login: item.date_login,
            expires_in: item.expires_in,
            token: item.token,
            token_type: item.token_type,
            user: item.user,
          };
        })
        .catch((err) => {
          throw new HttpException(err, HttpStatus.INTERNAL_SERVER_ERROR);
        });
    } catch ({ response, status }) {
      throw new HttpException(response, status);
    }
  }
  async comparePassword(password, passwordDb) {
    return await compareSync(password, passwordDb);
  }
  async register(user: RegisterDto): Promise<User> {
    try {
      const createdUser = await this.usersService.create({
        ...user,
      });
      createdUser.password = undefined;
      return createdUser;
    } catch (error) {
      if (error?.code == PostgresErrorCode.UniqueViolation) {
        throw new HttpException(
          'User with that email already exists',
          HttpStatus.BAD_REQUEST,
        );
      }
      throw new HttpException(
        'Something went wrong',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
  async changePassword(newpassword: NewPasswordModel): Promise<User> {
    const { token } = newpassword;
    const authDB = await this.authDb.findOne(
      { token: token },
      { relations: ['user'] },
    );
    if (!authDB) throw new UnauthorizedException('Session invalid');
    const userDB = await this.usersService.findOne(authDB.user.id);
    if (!userDB) throw new UnauthorizedException('User not found');
    newpassword.email = userDB.email;
    return this.usersService.changePassword(newpassword);
  }
  async verifyPassword(verify: VerifyPasswordDto): Promise<token> {
    const userDB = await this.usersService.getByEmail(verify.email);

    if (!userDB) throw new NotFoundException('Email not found');
    const payload = {
      id: String(userDB.id),
      email: userDB.email,
    };
    const body = {
      date_login: new Date(),
      token_type: 'Bearer',
      token: this.jwtService.sign(payload, {
        expiresIn: '1d',
      }),
      expires_in: (24 * 60 * 60).toString(),
      user: userDB,
    };

    return await this.authDb.save(body).then((item) => {
      return {
        token: item.token,
      };
    });
  }
}
