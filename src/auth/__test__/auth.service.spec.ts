import { JwtModule, JwtService } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { UsersModule } from '../../users/users.module';
import { Repository } from 'typeorm';
import { User } from '../../users/entities/user.entity';
import { UsersService } from '../../users/users.service';
import { AuthService } from '../auth.service';
import { jwtConstants } from '../constants';
import { Auth } from '../entities/auth.entity';
const userDto = {
  email: 'tes@test.com',
  first_name: 'test',
  last_name: 'test',
  password: '12345678',
};
const userReponse = {
  email: 'tes@test.com',
  first_name: 'test',
  last_name: 'test',
  rol: 'User',
  id: 'uuid',
  password: undefined,
  status: 'active',
  created_at: 'string',
  updated_at: 'string',
};
const authResponse = {
  date_login: 'Date',
  token_type: 'string',
  token: 'string',
  expires_in: 'string',
};
describe('AuthService', () => {
  let service: AuthService;
  let authRepo: Repository<Auth>;
  const mockedJwtService = {
    sign: () => '',
  };
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthService,
        {
          provide: getRepositoryToken(Auth),
          useValue: {
            save: jest
              .fn()
              .mockImplementation(() => Promise.resolve(authResponse)),
            comparePassword: jest
              .fn()
              .mockResolvedValue((password, repassword) => true),
          },
        },
        { provide: JwtService, useValue: mockedJwtService },
        {
          provide: UsersService,
          useValue: {
            create: jest
              .fn()
              .mockImplementation((userDto: CreateUserDto) => userReponse),
            getByEmail: jest
              .fn()
              .mockImplementation((email: string) => userReponse),
          },
        },
      ],
    }).compile();

    service = module.get<AuthService>(AuthService);
    authRepo = module.get<Repository<Auth>>(getRepositoryToken(Auth));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
  describe('register a new user', () => {
    it('should registered a new user', async () => {
      const newUser = {
        email: 'tes@test.com',
        first_name: 'test',
        last_name: 'test',
        password: '12345678',
      };

      await expect(service.register(newUser)).resolves.toEqual(userReponse);
    });
  });
  describe('Login Auth', () => {
    it('should login a user', async () => {
      const login = {
        email: 'test@test.com',
        password: '1234567',
      };
      await expect(service.login(login)).resolves.toEqual(authResponse);
      expect(authRepo.save).toBeCalledTimes(1);
      expect(authRepo.save).toBeCalledWith({
        date_login: expect.any(Date),
        expires_in: expect.any(String),
        token: expect.any(String),
        token_type: expect.any(String),
        user: login,
      });
    });
  });
});
