import { JwtService } from '@nestjs/jwt';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { CreateUserDto } from 'src/users/dto/create-user.dto';
import { User } from '../../users/entities/user.entity';
import { UsersService } from '../../users/users.service';
import { AuthController } from '../auth.controller';
import { AuthService } from '../auth.service';
import { LoginDto } from '../dto/login-auth.dto';
import { Auth } from '../entities/auth.entity';
const userDto = {
  email: 'tes@test.com',
  first_name: 'test',
  last_name: 'test',
  password: '12345678',
};
const userReponse = {
  email: 'tes@test.com',
  first_name: 'test',
  last_name: 'test',
  rol: 'User',
  id: 'uuid',
  status: 'active',
  created_at: new Date(),
  updated_at: new Date(),
};
const authResponse = {
  date_login: 'Date',
  token_type: 'string',
  token: 'string',
  expires_in: 'string',
};
describe('AuthController', () => {
  let controller: AuthController;
  let authService: AuthService;
  let usersService: UsersService;
  const mockedJwtService = {
    sign: () => '',
  };
  const mockAuthService = {
    save: jest.fn().mockImplementation(() => Promise.resolve('')),
    register: jest.fn().mockImplementation(() => Promise.resolve(userReponse)),
    login: jest.fn().mockImplementation(() => Promise.resolve(authResponse)),
  };
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AuthController],
      providers: [
        UsersService,
        AuthService,
        { provide: JwtService, useValue: mockedJwtService },
        {
          provide: getRepositoryToken(User),
          useValue: {
            create: jest.fn().mockResolvedValue((dto) => dto),
            find: jest
              .fn()
              .mockImplementation((id: string) =>
                Promise.resolve({ id, userDto }),
              ),
            save: jest
              .fn()
              .mockImplementation((userDto: CreateUserDto) =>
                Promise.resolve({ id: '', userDto }),
              ),
          },
        },
        {
          provide: getRepositoryToken(Auth),
          useValue: {
            save: jest.fn().mockImplementation(() => Promise.resolve('')),
          },
        },
      ],
    })
      .overrideProvider(AuthService)
      .useValue(mockAuthService)
      .compile();

    controller = module.get<AuthController>(AuthController);
    authService = module.get<AuthService>(AuthService);
    usersService = module.get<UsersService>(UsersService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
  describe('auth register ', () => {
    it('should retur a new user', async () => {
      await expect(controller.register(userDto)).resolves.toEqual(userReponse);
      expect(authService.register).toBeCalledTimes(1);
    });
  });
  describe('auth login ', () => {
    it('should authenticated a user', async () => {
      const logindto: LoginDto = {
        email: 'test@gmail.com',
        password: '12345678',
      };
      await expect(controller.login(logindto)).resolves.toEqual(authResponse);
      expect(authService.login).toBeCalledTimes(1);
    });
  });
});
