import { User } from 'src/users/entities/user.entity';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  ManyToOne,
} from 'typeorm';
@Entity('auth')
export class Auth {
  @PrimaryGeneratedColumn('uuid')
  id?: string;

  @CreateDateColumn({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  date_login: Date;

  @Column({ type: 'text' })
  token_type: string;

  @Column({ type: 'text' })
  token: string;

  @Column()
  expires_in: string;
  @ManyToOne(() => User, (user) => user.id, {
    onDelete: 'CASCADE',
  })
  user: User;

  //   @ManyToOne(() => UserEntity, (user) => user.id)
  //   user_?: UserEntity | object;
}
