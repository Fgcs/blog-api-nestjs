import { Test, TestingModule } from '@nestjs/testing';
import { CreateUserDto } from '../dto/create-user.dto';
import { UpdateUserDto } from '../dto/update-user.dto';
import { User } from '../entities/user.entity';
import { UsersController } from '../users.controller';
import { UsersService } from '../users.service';
const users = [
  {
    id: 'test1',
    last_name: 'test User',
    first_name: 'string',
    email: 'test@gmail.com',
    rol: 'Admin',
    status: 'Actived',
    created_at: new Date(),
    updated_at: new Date(),
  },
  {
    id: 'test2',
    last_name: 'test User 2',
    first_name: 'string',
    email: 'test2@gmail.com',
    rol: 'User',
    status: 'Actived',
    created_at: new Date(),
    updated_at: new Date(),
  },
];
const MockUser = {
  last_name: 'test User',
  first_name: 'string',
  email: 'test@test.com',
  rol: 'Admin',
  status: 'Actived',
};
describe('UsersController', () => {
  let controller: UsersController;
  let service: UsersService;
  const serviceMock = {
    findAll: jest.fn().mockResolvedValue(users),
    findOne: jest
      .fn()
      .mockImplementation((id: string) => Promise.resolve({ ...MockUser, id })),
    create: jest
      .fn()
      .mockImplementation((user: CreateUserDto) =>
        Promise.resolve({ id: 'uuid', ...user }),
      ),
    getByEmail: jest
      .fn()
      .mockImplementation((email: string) => Promise.resolve(MockUser)),
    update: jest.fn().mockImplementation((id: string, user: UpdateUserDto) =>
      Promise.resolve({
        ...user,
        id,
      }),
    ),
    remove: jest
      .fn()
      .mockImplementation((id: string) => Promise.resolve({ ...MockUser, id })),
  };
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [UsersService],
    })
      .overrideProvider(UsersService)
      .useValue(serviceMock)
      .compile();

    controller = module.get<UsersController>(UsersController);
    service = module.get<UsersService>(UsersService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
  describe('find All users ', () => {
    it('should get an array of users', async () => {
      await expect(controller.findAll()).resolves.toEqual(users);
    });
  });
  describe('find one user', () => {
    it('should get a singed user', async () => {
      await expect(controller.findOne('uuid')).resolves.toEqual({
        ...MockUser,
        id: expect.any(String),
      });
      await expect(controller.findOne('a different uuid')).resolves.toEqual({
        ...MockUser,
        id: 'a different uuid',
      });
    });
  });
  describe('get by Email', () => {
    it('should get a user by email', async () => {
      await expect(controller.getByEmail('test@test.com')).resolves.toEqual(
        MockUser,
      );
      const getByEmailSpy = jest
        .spyOn(service, 'getByEmail')
        .mockResolvedValueOnce(
          Promise.resolve({
            id: 'uid',
            email: 'test@test.com',
            first_name: 'string',
            last_name: 'test User',
            rol: 'Admin',
            status: 'Actived',
          }) as unknown as Promise<User>,
        );

      await expect(controller.getByEmail('test@test.com')).resolves.toEqual({
        email: 'test@test.com',
        first_name: 'string',
        last_name: 'test User',
        rol: 'Admin',
        status: 'Actived',
        id: 'uid',
      });
      expect(getByEmailSpy).toBeCalledWith('test@test.com');
    });
  });
  describe('create user', () => {
    it('should create a new user', async () => {
      const newUser: CreateUserDto = {
        first_name: 'test',
        last_name: 'test',
        email: 'test@test.com',
        password: 'newPassword',
      };
      await expect(controller.create(newUser)).resolves.toEqual({
        id: 'uuid',
        ...newUser,
      });
    });
  });
  describe('update user', () => {
    it('should update a user', async () => {
      const newUser: CreateUserDto = {
        first_name: 'test',
        last_name: 'test',
        email: 'test@test.com',
        password: 'newPassword',
      };
      await expect(controller.update('update user', newUser)).resolves.toEqual({
        ...newUser,
        id: 'update user',
      });
    });
  });
  describe('delete user', () => {
    it('should remove a user', async () => {
      await expect(controller.remove('uid')).resolves.toEqual({
        ...MockUser,
        id: 'uid',
      });
    });
    it('should return that it did not delete a user', async () => {
      const deleteSpy = jest
        .spyOn(service, 'remove')
        .mockResolvedValueOnce(
          Promise.resolve(MockUser) as unknown as Promise<User>,
        );
      await expect(
        controller.remove('a uuid that does not exist'),
      ).resolves.toEqual(MockUser);
      expect(deleteSpy).toBeCalledWith('a uuid that does not exist');
    });
  });
});
