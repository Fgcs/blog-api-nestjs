import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserDto } from '../dto/create-user.dto';
import { UpdateUserDto } from '../dto/update-user.dto';
import { User } from '../entities/user.entity';
import { UsersService } from '../users.service';

const oneUser = {
  last_name: 'test User',
  first_name: 'string',
  email: 'test@test.com',
  rol: 'Admin',
  status: 'Actived',
  password: undefined,
};
const usersMock = [
  {
    id: 'test1',
    last_name: 'test User',
    first_name: 'string',
    email: 'test@gmail.com',
    rol: 'Admin',
    status: 'Actived',
    created_at: new Date(),
    updated_at: new Date(),
  },
  {
    id: 'test2',
    last_name: 'test User 2',
    first_name: 'string',
    email: 'test2@gmail.com',
    rol: 'User',
    status: 'Actived',
    created_at: new Date(),
    updated_at: new Date(),
  },
];
const userDto = {
  first_name: 'test dto',
  last_name: 'test dto',
  email: 'test@test.com',
  password: 'testdto1234',
};
describe('UsersService', () => {
  let service: UsersService;
  let repo: Repository<User>;
  const mockUserRepository = {
    create: jest.fn().mockImplementation((user: CreateUserDto) => user),
    save: jest
      .fn()
      .mockImplementation((user: CreateUserDto) =>
        Promise.resolve({ id: 'uuid', ...user }),
      ),
    find: jest.fn().mockImplementation(() => Promise.resolve(usersMock)),
    findOne: jest
      .fn()
      .mockImplementation((id: string) => Promise.resolve({ ...oneUser })),
    update: jest
      .fn()
      .mockImplementation((id: string, user: UpdateUserDto) =>
        Promise.resolve({ id, ...user }),
      ),
    delete: jest
      .fn()
      .mockImplementation((id: string) => Promise.resolve(oneUser)),
  };
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UsersService,
        {
          provide: getRepositoryToken(User),
          useValue: mockUserRepository,
        },
      ],
    }).compile();

    service = module.get<UsersService>(UsersService);

    repo = module.get<Repository<User>>(getRepositoryToken(User));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
  describe('create user', () => {
    it('should successfully insert  user ', async () => {
      await expect(service.create(userDto)).resolves.toEqual({
        id: expect.any(String),
        ...userDto,
      });
      expect(repo.create).toBeCalledTimes(1);
      expect(repo.create).toBeCalledWith(userDto);
      expect(repo.save).toBeCalledTimes(1);
    });
  });
  describe('get all', () => {
    it('should return an array of users', async () => {
      const users = await service.findAll();
      expect(users).toEqual(usersMock);
      expect(repo.find).toBeCalledTimes(1);
    });
  });
  describe('get One', () => {
    it('should ge a single user', async () => {
      const repoSpy = jest.spyOn(repo, 'findOne');
      await expect(service.findOne('uuid')).resolves.toEqual(oneUser);
      expect(repoSpy).toBeCalledWith('uuid');
      expect(repo.findOne).toBeCalledTimes(1);
    });
  });
  describe('getOneByEmail', () => {
    it('should get one user', async () => {
      const repoSp = jest.spyOn(repo, 'findOne');
      await expect(service.getByEmail('test@test.com')).resolves.toEqual(
        oneUser,
      );
      // expect(repoSp).toBeCalledWith({ email: 'test@test.com' });
      // expect(repo.findOne).toBeCalledTimes(1);
    });
  });
  describe('updateOne', () => {
    it('should call the update method', async () => {
      const user = await service.update('uuid', {
        first_name: userDto.first_name,
        last_name: userDto.last_name,
      });
      expect(user).toEqual(oneUser);
      expect(repo.update).toBeCalledTimes(1);
      expect(repo.update).toBeCalledWith('uuid', {
        first_name: userDto.first_name,
        last_name: userDto.last_name,
      });
    });
  });
  describe('delete one', () => {
    it('should return the deleted user ', async () => {
      await expect(service.remove('string')).resolves.toEqual(oneUser);
    });
  });
});
