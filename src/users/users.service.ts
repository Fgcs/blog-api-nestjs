import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { NewPasswordModel } from 'src/auth/dto/newPassword.dto';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';
import { hashSync } from 'bcrypt';
@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private readonly userDb: Repository<User>,
  ) {}
  async create(userData: CreateUserDto): Promise<User> {
    const newUser = await this.userDb.create(userData);
    return await this.userDb.save(newUser);
  }

  async getByEmail(email: string): Promise<User> {
    const user = await this.userDb.findOne({
      where: { email: email.toLowerCase() },
    });
    if (!user) throw new HttpException('user not found', HttpStatus.NOT_FOUND);
    return user;
  }
  async findAll(): Promise<User[]> {
    return await this.userDb.find().then((res) => {
      return res.map((item) => {
        item.password = undefined;
        return item;
      });
    });
  }

  async findOne(id: string) {
    return await this.userDb
      .findOne(id)
      .then((item) => {
        item.password = undefined;
        return item;
      })
      .catch((err) => {
        throw new HttpException('user not found', HttpStatus.NOT_FOUND);
      });
  }

  async update(id: string, updateUserDto: UpdateUserDto): Promise<User> {
    const dataUpdate = await this.userDb.update(id, { ...updateUserDto });
    return this.userDb.findOne(id);
  }
  async changePassword(newPassword: NewPasswordModel): Promise<User> {
    const user = await this.userDb.findOneOrFail({ email: newPassword.email });

    const dataUpdate = await this.userDb.update(user.id, {
      password: await hashSync(newPassword.password, 10),
    });
    user.password = undefined;
    return user;
  }
  async remove(id: string): Promise<User> {
    const user = await this.userDb.findOne(id);
    await this.userDb.delete(id).catch((er) => {
      throw new HttpException('user not found ', HttpStatus.NOT_FOUND);
    });
    return user;
  }
}
