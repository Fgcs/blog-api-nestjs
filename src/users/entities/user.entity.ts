import {
  BeforeInsert,
  BeforeUpdate,
  Column,
  CreateDateColumn,
  Entity,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
  Unique,
  UpdateDateColumn,
} from 'typeorm';
import { hashSync } from 'bcrypt';
import { Article } from '../../article/entities/article.entity';
@Entity('user')
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ type: 'text' })
  first_name: string;

  @Column({ type: 'text' })
  last_name: string;
  @Unique(['email'])
  @Column({ type: 'text' })
  email: string;

  @Column({ type: 'text' })
  password: string;
  @Column({
    type: 'enum',
    enum: ['user', 'admin', 'superAdmin'],
    default: 'user',
  })
  rol: string;

  @Column({
    type: 'enum',
    enum: ['active', 'inactive', 'blocked'],
    default: 'active',
  })
  status: string;
  @OneToMany((type) => Article, (article) => article.author)
  articles?: Article[];

  @CreateDateColumn({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  created_at: Date;
  @UpdateDateColumn({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  updated_at: Date;

  @BeforeInsert()
  @BeforeUpdate()
  async hashPassword() {
    if (!this.password) {
      return;
    }
    this.password = await hashSync(this.password, 10);
  }
  @BeforeInsert()
  @BeforeUpdate()
  async lowerCaseEmail() {
    if (!this.email) {
      return;
    }
    this.email = this.email.toLowerCase();
  }
}
