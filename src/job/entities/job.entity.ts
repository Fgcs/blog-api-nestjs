import {
  BeforeInsert,
  BeforeUpdate,
  Column,
  CreateDateColumn,
  Entity,
  ManyToMany,
  OneToMany,
  PrimaryGeneratedColumn,
  Unique,
  UpdateDateColumn,
} from 'typeorm';
@Entity('job')
export class Job {
  @PrimaryGeneratedColumn('uuid')
  id?: string;
  @Column({ type: 'text' })
  company?: string;
  @Column({ type: 'text' })
  address?: string;
  @Column({ type: 'text' })
  email?: string;
  @Column({ type: 'text' })
  position?: string;
  @Column({ type: 'text', default: '' })
  phone?: string;
  @Column({ type: 'text' })
  optional?: string;
  @Column({ type: 'text' })
  time?: string;
  @Column({ type: 'text' })
  description?: string;
  @CreateDateColumn({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  created_at: Date;
  @UpdateDateColumn({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  updated_at: Date;
}
