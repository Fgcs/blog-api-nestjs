import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateJobDto } from './dto/create-job.dto';
import { UpdateJobDto } from './dto/update-job.dto';
import { Job } from './entities/job.entity';

@Injectable()
export class JobService {
  constructor(@InjectRepository(Job) private readonly jobDb: Repository<Job>) {}
  async create(createJobDto: CreateJobDto) {
    const newJob = await this.jobDb.create(createJobDto);
    return await this.jobDb.save(newJob);
  }

  async findAll(): Promise<Job[]> {
    return await this.jobDb.find();
  }

  async findOne(id: string): Promise<Job> {
    return await this.jobDb.findOne(id);
  }

  async update(id: string, updateJobDto: UpdateJobDto): Promise<Job> {
    const dataUpdate = await this.jobDb.update(id, { ...updateJobDto });
    return this.jobDb.findOne(id);
  }

  async remove(id: string): Promise<Job> {
    const job = await this.jobDb.findOne(id);
    await this.jobDb.delete(id);
    return job;
  }
}
