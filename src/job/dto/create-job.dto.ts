import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsNotEmpty, IsOptional, IsString } from 'class-validator';
export class CreateJobDto {
  @IsNotEmpty()
  @ApiProperty()
  company?: string;
  @IsString()
  @ApiProperty()
  address?: string;
  @IsNotEmpty()
  @ApiProperty()
  email?: string;
  @IsString()
  @ApiProperty()
  position?: string;
  @IsString()
  @ApiProperty()
  phone?: string;
  @IsString()
  @ApiProperty()
  optional?: string;
  @IsString()
  @ApiProperty()
  time?: string;
  @IsString()
  @ApiProperty()
  description?: string;
}
