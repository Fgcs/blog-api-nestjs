import { Test, TestingModule } from '@nestjs/testing';
import { ArticleController } from '../article.controller';
import { ArticleService } from '../article.service';
import { Article } from '../entities/article.entity';
const MockUser = {
  id: 'test1',
  last_name: 'test User',
  password: '12345',
  first_name: 'string',
  email: 'test@gmail.com',
  rol: 'Admin',
  status: 'Actived',
  created_at: new Date(),
  updated_at: new Date(),
};
const articles = {
  id: 'string',
  slug: 'string',
  title: 'string',
  description: 'string',
  body: 'string',
  author: MockUser,
  tagList: ['string'],
  created_at: new Date(),
  updated_at: new Date(),
};

describe('ArticleController', () => {
  let controller: ArticleController;
  let service: ArticleService;
  const serviceMock = {
    findAll: jest.fn().mockResolvedValue(articles),
  };
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ArticleController],
      providers: [ArticleService],
    })
      .overrideProvider(ArticleService)
      .useValue(serviceMock)
      .compile();

    controller = module.get<ArticleController>(ArticleController);
    service = module.get<ArticleService>(ArticleService);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
  describe('Find all articles', () => {
    it('should get a array of articles', async () => {
      await expect(controller.findAll()).resolves.toEqual(articles);
    });
  });
});
