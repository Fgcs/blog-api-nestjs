import { User } from 'src/users/entities/user.entity';

export class FavoriteArticleDto {
  user: User;
}
