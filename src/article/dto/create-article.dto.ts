import { ApiProperty } from '@nestjs/swagger';
import { IsArray, IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { User } from '../../users/entities/user.entity';

export class CreateArticleDto {
  @IsString()
  @ApiProperty()
  title: string;
  @IsString()
  @ApiProperty()
  description: string;
  @IsNotEmpty()
  author: string;
  @IsNotEmpty()
  image?: string;
  @IsOptional()
  status: string = 'public' || 'draf';
  // @IsArray()
  // @ApiProperty()
  // tagList: string[];
}
