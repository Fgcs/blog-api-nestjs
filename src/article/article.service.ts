import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UsersService } from 'src/users/users.service';
import { Repository } from 'typeorm';
import { CreateArticleDto } from './dto/create-article.dto';
import { FavoriteArticleDto } from './dto/favorite-article.dto';
import { UpdateArticleDto } from './dto/update-article.dto';
import { Article } from './entities/article.entity';
import { TagEntity } from './entities/tags.entity';

@Injectable()
export class ArticleService {
  constructor(
    @InjectRepository(Article) private readonly articleDb: Repository<Article>,
    @InjectRepository(TagEntity) private tagRepo: Repository<TagEntity>,
    private userService: UsersService,
  ) {}
  private async upsertTags(tagList: string[]): Promise<void> {
    const foundTags = await this.tagRepo.find({
      where: tagList.map((t) => ({ tag: t })),
    });
    const newTags = tagList.filter(
      (t) => !foundTags.map((t) => t.tag).includes(t),
    );
    const tags = this.tagRepo.create(newTags.map((t) => ({ tag: t })));
    await Promise.all(tags.map((tag) => this.tagRepo.save(tag)));
  }
  async create(createArticleDto: CreateArticleDto) {
    const user = await this.userService.findOne(createArticleDto.author);

    const article = this.articleDb.create({
      author: user,
      description: createArticleDto.description,
      title: createArticleDto.title,
      image: createArticleDto.image,
    });
    // await this.upsertTags(createArticleDto.tagList);
    return await this.articleDb.save(article);
  }
  async findBySlug(slug: string): Promise<Article> {
    return await this.articleDb.findOne({
      where: { slug },
    });
  }
  async findAll(): Promise<Article[]> {
    return await this.articleDb.find({ relations: ['author'] });
  }

  async findOne(id: string): Promise<Article> {
    return await this.articleDb.findOne(id, { relations: ['author'] });
  }
  async getBySlug(slug: string): Promise<Article> {
    return await this.articleDb.findOne(
      { slug: slug },
      { relations: ['author'] },
    );
  }
  async update(
    id: string,
    updateArticleDto: UpdateArticleDto,
  ): Promise<Article> {
    await this.articleDb.update(id, {
      ...updateArticleDto,
    });
    return await this.articleDb.findOne(id);
  }
  // async favoriteArticle(
  //   slug: string,
  //   favorite: FavoriteArticleDto,
  // ): Promise<Article> {
  //   const article = await this.findBySlug(slug);
  //   article.favoritedBy.push(favorite.user);
  //   await this.articleDb.update(article.id, {
  //     favoritedBy: article.favoritedBy,
  //   });
  //   return await this.findBySlug(slug);
  // }
  async remove(id: string): Promise<Article> {
    const article = await this.articleDb.findOne(id);
    await this.articleDb.delete(id);
    return article;
  }
}
