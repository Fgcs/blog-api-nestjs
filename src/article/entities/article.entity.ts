import {
  BeforeInsert,
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import * as slugify from 'slug';
import { User } from '../../users/entities/user.entity';
@Entity()
export class Article {
  @PrimaryGeneratedColumn('uuid')
  id?: string;
  @Column({ type: 'text', default: null })
  slug?: string;

  @Column({ type: 'text', default: null })
  title?: string;

  @Column({ type: 'text', default: null })
  description?: string;

  @Column({
    type: 'enum',
    enum: ['public', 'draf', 'blocked'],
    default: 'public',
  })
  status?: string = 'public' || 'draf';
  @Column({ type: 'text', default: null })
  image?: string;
  // @ManyToMany((type) => User, (user) => user.favorites, { eager: true })
  // @JoinTable()
  // favoritedBy?: User[];

  //   @OneToMany((type) => CommentEntity, (comment) => comment.article)
  //   comments: CommentEntity[];

  @ManyToOne((type) => User, (user) => user.articles, { eager: true })
  author?: User;

  // @Column('simple-array')
  // tagList: string[];
  @CreateDateColumn({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  created_at?: Date;
  @UpdateDateColumn({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  updated_at?: Date;
  @BeforeInsert()
  generateSlug() {
    this.slug =
      slugify(this.title, { lower: true }) +
      '-' +
      ((Math.random() * Math.pow(36, 6)) | 0).toString(36);
  }
}
